﻿
namespace proyecto_tienda
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Resident = new System.Windows.Forms.CheckBox();
            this.Apex = new System.Windows.Forms.CheckBox();
            this.minecraft = new System.Windows.Forms.CheckBox();
            this.fornite = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CallDuty = new System.Windows.Forms.CheckBox();
            this.Halo = new System.Windows.Forms.CheckBox();
            this.Cyber = new System.Windows.Forms.CheckBox();
            this.Mortal = new System.Windows.Forms.CheckBox();
            this.Good = new System.Windows.Forms.CheckBox();
            this.LittleNight = new System.Windows.Forms.CheckBox();
            this.JustCause = new System.Windows.Forms.CheckBox();
            this.AsCreed = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.Controls.Add(this.pictureBox5);
            this.groupBox1.Controls.Add(this.comboBox4);
            this.groupBox1.Controls.Add(this.comboBox3);
            this.groupBox1.Controls.Add(this.pictureBox4);
            this.groupBox1.Controls.Add(this.comboBox2);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.pictureBox3);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.Resident);
            this.groupBox1.Controls.Add(this.Apex);
            this.groupBox1.Controls.Add(this.minecraft);
            this.groupBox1.Controls.Add(this.fornite);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(27, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(252, 491);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "juegos populares";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::proyecto_tienda.Properties.Resources._1153181620343736;
            this.pictureBox5.Location = new System.Drawing.Point(137, 380);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(82, 105);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 2;
            this.pictureBox5.TabStop = false;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "PC",
            "XBOX",
            "PS4",
            "SWITCH"});
            this.comboBox4.Location = new System.Drawing.Point(6, 389);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(91, 21);
            this.comboBox4.TabIndex = 2;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "PC",
            "XBOX",
            "PS4",
            "SWITCH"});
            this.comboBox3.Location = new System.Drawing.Point(6, 300);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(91, 21);
            this.comboBox3.TabIndex = 2;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::proyecto_tienda.Properties.Resources._714145881549357139;
            this.pictureBox4.Location = new System.Drawing.Point(137, 264);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(82, 100);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 2;
            this.pictureBox4.TabStop = false;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "PC",
            "XBOX",
            "PS4",
            "SWITCH"});
            this.comboBox2.Location = new System.Drawing.Point(6, 163);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(91, 21);
            this.comboBox2.TabIndex = 2;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "PC",
            "XBOX ONE",
            "PS4",
            "SWITCH"});
            this.comboBox1.Location = new System.Drawing.Point(6, 68);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(91, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::proyecto_tienda.Properties.Resources._1b9f772d10ae2cfce18fab1b05705810;
            this.pictureBox3.Location = new System.Drawing.Point(137, 155);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(82, 86);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::proyecto_tienda.Properties.Resources.wTKD65TjfkPGDlZfh_MhRATA0svz9fxjEHEAM6_mtlw_350x200_1x_0;
            this.pictureBox2.Location = new System.Drawing.Point(137, 19);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(82, 106);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // Resident
            // 
            this.Resident.AutoSize = true;
            this.Resident.Location = new System.Drawing.Point(6, 353);
            this.Resident.Name = "Resident";
            this.Resident.Size = new System.Drawing.Size(123, 30);
            this.Resident.TabIndex = 2;
            this.Resident.Text = "Resident evil village \r\n$ 1200";
            this.Resident.UseVisualStyleBackColor = true;
            // 
            // Apex
            // 
            this.Apex.AutoSize = true;
            this.Apex.Location = new System.Drawing.Point(6, 264);
            this.Apex.Name = "Apex";
            this.Apex.Size = new System.Drawing.Size(117, 30);
            this.Apex.TabIndex = 2;
            this.Apex.Text = "Apex legends pack\r\n de leyendas $ 400";
            this.Apex.UseVisualStyleBackColor = true;
            // 
            // minecraft
            // 
            this.minecraft.AutoSize = true;
            this.minecraft.Location = new System.Drawing.Point(6, 140);
            this.minecraft.Name = "minecraft";
            this.minecraft.Size = new System.Drawing.Size(100, 17);
            this.minecraft.TabIndex = 2;
            this.minecraft.Text = "Minecraft $ 400";
            this.minecraft.UseVisualStyleBackColor = true;
            // 
            // fornite
            // 
            this.fornite.AutoSize = true;
            this.fornite.Location = new System.Drawing.Point(6, 19);
            this.fornite.Name = "fornite";
            this.fornite.Size = new System.Drawing.Size(100, 43);
            this.fornite.TabIndex = 2;
            this.fornite.Text = "Fortnite pack \r\nsalvar el mundo\r\n$ 500\r\n";
            this.fornite.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::proyecto_tienda.Properties.Resources._0a60bc05b97180bf77a29d734d655223;
            this.pictureBox1.Location = new System.Drawing.Point(2, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(842, 551);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.groupBox2.Controls.Add(this.CallDuty);
            this.groupBox2.Controls.Add(this.Halo);
            this.groupBox2.Controls.Add(this.Cyber);
            this.groupBox2.Controls.Add(this.Mortal);
            this.groupBox2.Controls.Add(this.Good);
            this.groupBox2.Controls.Add(this.LittleNight);
            this.groupBox2.Controls.Add(this.JustCause);
            this.groupBox2.Controls.Add(this.AsCreed);
            this.groupBox2.Location = new System.Drawing.Point(371, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(446, 241);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "juegos de accion";
            // 
            // CallDuty
            // 
            this.CallDuty.AutoSize = true;
            this.CallDuty.Location = new System.Drawing.Point(162, 191);
            this.CallDuty.Name = "CallDuty";
            this.CallDuty.Size = new System.Drawing.Size(127, 30);
            this.CallDuty.TabIndex = 7;
            this.CallDuty.Text = "Call of duty could war\r\n$ 1400";
            this.CallDuty.UseVisualStyleBackColor = true;
            // 
            // Halo
            // 
            this.Halo.AutoSize = true;
            this.Halo.Location = new System.Drawing.Point(162, 135);
            this.Halo.Name = "Halo";
            this.Halo.Size = new System.Drawing.Size(153, 30);
            this.Halo.TabIndex = 6;
            this.Halo.Text = "Halo master chief colletion \r\n$ 800";
            this.Halo.UseVisualStyleBackColor = true;
            // 
            // Cyber
            // 
            this.Cyber.AutoSize = true;
            this.Cyber.Location = new System.Drawing.Point(162, 82);
            this.Cyber.Name = "Cyber";
            this.Cyber.Size = new System.Drawing.Size(107, 30);
            this.Cyber.TabIndex = 5;
            this.Cyber.Text = "Cyberpunk 2077 \r\n$ 1200";
            this.Cyber.UseVisualStyleBackColor = true;
            // 
            // Mortal
            // 
            this.Mortal.AutoSize = true;
            this.Mortal.Location = new System.Drawing.Point(162, 32);
            this.Mortal.Name = "Mortal";
            this.Mortal.Size = new System.Drawing.Size(111, 30);
            this.Mortal.TabIndex = 4;
            this.Mortal.Text = "Mortal kombat 11 \r\n$ 900";
            this.Mortal.UseVisualStyleBackColor = true;
            // 
            // Good
            // 
            this.Good.AutoSize = true;
            this.Good.Location = new System.Drawing.Point(17, 191);
            this.Good.Name = "Good";
            this.Good.Size = new System.Drawing.Size(114, 17);
            this.Good.TabIndex = 3;
            this.Good.Text = "Good of war $ 900";
            this.Good.UseVisualStyleBackColor = true;
            // 
            // LittleNight
            // 
            this.LittleNight.AutoSize = true;
            this.LittleNight.Location = new System.Drawing.Point(17, 128);
            this.LittleNight.Name = "LittleNight";
            this.LittleNight.Size = new System.Drawing.Size(111, 30);
            this.LittleNight.TabIndex = 2;
            this.LittleNight.Text = "Little nightmares 2\r\n $ 600";
            this.LittleNight.UseVisualStyleBackColor = true;
            // 
            // JustCause
            // 
            this.JustCause.AutoSize = true;
            this.JustCause.Location = new System.Drawing.Point(17, 89);
            this.JustCause.Name = "JustCause";
            this.JustCause.Size = new System.Drawing.Size(116, 17);
            this.JustCause.TabIndex = 1;
            this.JustCause.Text = "Just cause 4 $ 350";
            this.JustCause.UseVisualStyleBackColor = true;
            // 
            // AsCreed
            // 
            this.AsCreed.AutoSize = true;
            this.AsCreed.Location = new System.Drawing.Point(17, 32);
            this.AsCreed.Name = "AsCreed";
            this.AsCreed.Size = new System.Drawing.Size(105, 30);
            this.AsCreed.TabIndex = 0;
            this.AsCreed.Text = "Assansiins creed\r\nremake $ 500 ";
            this.AsCreed.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Location = new System.Drawing.Point(599, 304);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 166);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "precio total";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(49, 110);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(99, 32);
            this.button2.TabIndex = 2;
            this.button2.Text = "Cerrar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(49, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 36);
            this.button1.TabIndex = 1;
            this.button1.Text = "Calculo total";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(49, 74);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(99, 30);
            this.button3.TabIndex = 3;
            this.button3.Text = "otro juego";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 552);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "tienda de videojuegos";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.CheckBox Resident;
        private System.Windows.Forms.CheckBox Apex;
        private System.Windows.Forms.CheckBox minecraft;
        private System.Windows.Forms.CheckBox fornite;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox CallDuty;
        private System.Windows.Forms.CheckBox Halo;
        private System.Windows.Forms.CheckBox Cyber;
        private System.Windows.Forms.CheckBox Mortal;
        private System.Windows.Forms.CheckBox Good;
        private System.Windows.Forms.CheckBox LittleNight;
        private System.Windows.Forms.CheckBox JustCause;
        private System.Windows.Forms.CheckBox AsCreed;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
    }
}

