﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace proyecto_tienda
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            conexion.conectar();
            string insertar = "INSERT INTO TIENDA (nombre,precio,plataforma,compañia)VALUES(@nombre,@precio,@plataforma,@compañia)";
            SqlCommand cmd1 = new SqlCommand(insertar, conexion.conectar());
            cmd1.Parameters.AddWithValue("@nombre", textBox1.Text);
            cmd1.Parameters.AddWithValue("@precio", textBox2.Text);
            cmd1.Parameters.AddWithValue("@plataforma", textBox3.Text);
            cmd1.Parameters.AddWithValue("@compañia", textBox4.Text);

            cmd1.ExecuteNonQuery();

            MessageBox.Show("el producto fue agregado con exito");

            dataGridView1.DataSource = llenar_grid();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                textBox3.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                textBox4.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            }
            catch { }
            int n = e.RowIndex;

            if(n!=-1)
            {
                label5.Text = (string)dataGridView1.Rows[n].Cells[1].Value;
            }


        }

        private void Form2_Load(object sender, EventArgs e)
        {
            conexion.conectar();
            MessageBox.Show("conexion exitosa");

            dataGridView1.DataSource = llenar_grid();
        }

        public DataTable llenar_grid()
        {
            conexion.conectar();
            DataTable dt = new DataTable();
            string consulta = "SELECT * FROM TIENDA";
            SqlCommand cmd = new SqlCommand(consulta, conexion.conectar());

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            da.Fill(dt);
            return dt;
                 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            conexion.conectar();
            string eliminar = "DELETE FROM TIENDA WHERE nombre=@nombre";
            SqlCommand cmd2 = new SqlCommand(eliminar, conexion.conectar());
            cmd2.Parameters.AddWithValue("@nombre", textBox1.Text);

            cmd2.ExecuteNonQuery();

            MessageBox.Show("el producto fue eliminado");

            dataGridView1.DataSource = llenar_grid();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            conexion.conectar();
            string actualizar = "UPDATE TIENDA SET nombre=@nombre,precio=@precio,plataforma=@plataforma,compañia=@compañia WHERE nombre=@nombre";
            SqlCommand cmd3 = new SqlCommand(actualizar, conexion.conectar());

            cmd3.Parameters.AddWithValue("@nombre", textBox1.Text);
            cmd3.Parameters.AddWithValue("@precio", textBox2.Text);
            cmd3.Parameters.AddWithValue("@plataforma", textBox3.Text);
            cmd3.Parameters.AddWithValue("@compañia", textBox4.Text);

            cmd3.ExecuteNonQuery();

            MessageBox.Show("le producto fue cambiado");
            dataGridView1.DataSource = llenar_grid();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
        }
    }
}
