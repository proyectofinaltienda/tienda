﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proyecto_tienda
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int total = 0;
            if (fornite.Checked == true)
            {
                total = total + 500;
            }
            if (minecraft.Checked == true)
            {
                total = total + 400;
            }
            if (Apex.Checked == true)
            {
                total = total + 400;
            }
            if (AsCreed.Checked == true)
            {
                total = total + 400;
            }
            if (Resident.Checked == true)
            {
                total = total + 1200;
            }
            if (CallDuty.Checked == true)
            {
                total = total + 1400;
            }
            if (Cyber.Checked == true)
            {
                total = total + 1200;
            }
            if (Good.Checked == true)
            {
                total = total + 900;
            }

            if (Halo.Checked == true)
            {
                total = total + 800;
            }
            if (JustCause.Checked == true)
            {
                total = total + 350;
            }
            if (LittleNight.Checked == true)
            {
                total = total + 600;
            }
            if (Mortal.Checked == true)
            {
                total = total + 900;
            }

            MessageBox.Show("El total es  $ " + total.ToString(), "Total");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form videojuego = new Form2();
            videojuego.Show();
        }
    }
}
